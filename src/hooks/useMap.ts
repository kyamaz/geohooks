import { useState, useEffect } from "react";
export const useMap = () => {
  const [mapbox, setMapbox] = useState(null);
  return { mapbox, setMapbox };
};
