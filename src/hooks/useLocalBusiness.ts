import {
  GeoPosition,
  makeGeoParams,
  geoIsSet
} from "./../shared/utils/location";
import { useState, useEffect } from "react";
import { YELP_API } from "src/interceptor/localBusiness.api";
import { SEARCH_BUSINESS } from "@shared/const";
import { setMapData } from "@shared/utils/mapbox";

const fetchRessource = (params: string) => {
  return YELP_API.get(`${SEARCH_BUSINESS}?${params}`).then(
    response => response.data
  );
};
export const useLocalBusiness = (geo: GeoPosition, queryParams: string) => {
  const [localBusiness, setLocalBusiness] = useState(null);
  useEffect(
    () => {
      if (geoIsSet(geo)) {
        const params = `${makeGeoParams(geo)}&categories=food${queryParams}`;
        fetchRessource(params).then(data => setLocalBusiness(setMapData(data)));
      }
    },
    [geo, queryParams]
  );
  return { localBusiness, setLocalBusiness };
};
