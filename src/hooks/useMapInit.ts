import { getUserPosition } from "@shared/utils/location";
import { centerMap } from "./../shared/utils/location";
import { useEffect } from "react";
import MapboxGl from "mapbox-gl/dist/mapbox-gl.js";
import { Map } from "mapbox-gl";
import { MAP_STYLE, MAP_TOKEN } from "@shared/const";
import {
  displayNavigationControl,
  displayScaleControl,
  userLocation
} from "@shared/utils/mapboxControls";
//initialze mapbox &&
function initMap(el: JSX.Element): Map {
  try {
    return new MapboxGl.Map({
      container: el,
      style: MAP_STYLE,
      zoom: 12 //for some reason, has to set zoom for center mpa to worl.whatever
    });
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
}
function initMapbox(el: JSX.Element) {
  if (el) {
    MapboxGl.accessToken = MAP_TOKEN;
    return initMap(el);
  }
  return null;
}

export function useMapInit(
  map: Map,
  props: { onSetMapBox?: Function; onSetUserPosition?: Function }
): void {
  useEffect(
    () => {
      if (map) {
        // props.onSetMapBox(map); //map mapbox instance available to parent
        // centerMap(map, props.onSetUserPosition);
        // userLocation(map);
        displayNavigationControl(map);
        displayScaleControl(map);
      }
    },
    [map]
  );
}

export const initialMapState = { map: null };
export const MAP_INIT = "MAP_INIT";
export function mapReducer(
  state: { map: Map },
  action: { type: string; payload: any }
) {
  switch (action.type) {
    case "reset":
      return initialMapState;
    case "MAP_INIT": {
      return { map: action.payload };
    }
    default:
      // A reducer must always return a valid state.
      // Alternatively you can throw an error if an invalid action is dispatched.
      return state;
  }
}

//
export const useDispatchMapInit = ({
  map,
  setMapbox,
  setMapControls,
  setGeoLocateCtrl,
  setCurrentPosition
}) => {
  useEffect(
    () => {
      if (map) {
        setMapbox(map);
        const geoCtrl = setMapControls(map);
        setTimeout(() => geoCtrl.trigger(), 100);
        setGeoLocateCtrl(geoCtrl);
        getUserPosition().then(pos => setCurrentPosition(pos));
      }
    },
    [map]
  );
};
