import { useReducer } from "react";
export const initialDataState = {
  isDataLoaded: false
};
export const DATA_LOADING: string = "DATA_LOADING";
export const DATA_LOADED: string = "DATA_LOADED";
export function dataReducer(state, action) {
  console.log(state);
  switch (action.type) {
    case "reset":
      return initialDataState;

    default:
      // A reducer must always return a valid state.
      // Alternatively you can throw an error if an invalid action is dispatched.
      return state;
  }
}
