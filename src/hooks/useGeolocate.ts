import { GeolocateControl } from "mapbox-gl";

import { useState, useEffect } from "react";

export const useGeoLocateCtrl = (geoLocate: GeolocateControl) => {
  const [geoLocateCtrl, setGeoLocateCtrl] = useState(null);
  useEffect(
    () => {
      if (!!geoLocate) {
        setGeoLocateCtrl(geoLocate);
      }
    },
    [geoLocate]
  );
  return { geoLocateCtrl, setGeoLocateCtrl };
};
