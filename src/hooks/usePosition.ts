import { useState, useEffect } from "react";
import { getUserPosition } from "@shared/utils/location";

export const useCurrentPosition = (render: any) => {
  const [currentPosition, setCurrentPosition] = useState({
    lat: 0,
    lon: 0
  });
  useEffect(
    () => {
      if (render) {
        console.log(render);
        setCurrentPosition(render);
        //getUserPosition().then(pos => setCurrentPosition(pos));
      }
    },
    [render]
  );

  return { currentPosition, setCurrentPosition };
};
