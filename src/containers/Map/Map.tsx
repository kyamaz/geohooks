import * as React from "react";
import { MapboxApi } from "@components/MapBoxApi/MapBoxApi";
import { Map } from "mapbox-gl";
import { useMap } from "@hooks/useMap";

import "react-sliding-pane/dist/react-sliding-pane.css";
//style
import styled from "styled-components";
import { useLocalBusiness } from "@hooks/useLocalBusiness";
import { useState, useReducer } from "react";
import { MapProvider } from "src/context/mapContext";
import { useCurrentPosition } from "@hooks/usePosition";
import { PrimaryBtn, StickyGroupBtn } from "src/Ui/btn";
import { MapModal } from "@components/MapModal/MapModal";
import { RenderLoading } from "src/Ui/loader";
import { useGeoLocateCtrl } from "@hooks/useGeolocate";
import { setMapControls } from "@shared/utils/mapboxControls";
import { getUserPosition } from "@shared/utils/location";
import {
  mapReducer,
  initialMapState,
  useDispatchMapInit
} from "@hooks/useMapInit";
import { useToggle } from "@hooks/useToggle";
import { AppToken } from "src/state/state";
const MapWrap = styled.div`
  display: flex;
  position: relative;
`;

interface MapProps {}

function setMapCtrls(map, cb) {
  const geoCtrl = setMapControls(map);
  setTimeout(() => geoCtrl.trigger(), 100);
  cb.call(null, geoCtrl);
}

function AppMap(props: MapProps, ref: any) {
  //state
  const [toggleState, handleUiToogle] = useToggle(false);
  const [mapQuery, setMapQuery] = useState("&sort_by=distance");
  const { geoLocateCtrl, setGeoLocateCtrl } = useGeoLocateCtrl(null);
  //hooks
  const { mapbox, setMapbox } = useMap();
  const { currentPosition, setCurrentPosition } = useCurrentPosition(null);
  const { localBusiness, setLocalBusiness } = useLocalBusiness(
    currentPosition,
    mapQuery
  );

  // props
  const handleOnSetMapBox = ({ map }: { map: Map }) => {
    setMapbox(map);
    setMapCtrls(map, setGeoLocateCtrl);
    getUserPosition().then(pos => setCurrentPosition(pos));
  };
  //ui event

  const setDefaultQuery = (e: React.SyntheticEvent) => {
    setMapQuery("&sort_by=distance");
  };
  const loading = !!localBusiness ? null : <RenderLoading />;
  const setCustomQuery = (e: React.SyntheticEvent): any => {
    setMapQuery("&sort_by=rating");
  };
  return (
    <div>
      {loading}
      <StickyGroupBtn>
        <PrimaryBtn onClick={setDefaultQuery}>default</PrimaryBtn>
        <PrimaryBtn onClick={setCustomQuery}>I feel lucky</PrimaryBtn>
        <PrimaryBtn onClick={() => handleUiToogle()}>filter detail </PrimaryBtn>
      </StickyGroupBtn>
      <MapModal
        isExpanded={toggleState}
        onCloseModal={() => handleUiToogle(false)}
        from="bottom"
      >
        <p>filters to come </p>
      </MapModal>
      <MapWrap>
        <MapProvider
          value={{
            data: localBusiness,
            mapbox,
            geoLocateCtrl,
            position: currentPosition
          }}
        >
          <MapboxApi onSetMapBox={handleOnSetMapBox} />
        </MapProvider>
      </MapWrap>
    </div>
  );
}

export default AppMap;
