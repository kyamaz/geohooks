import * as React from 'react'
import { AppState } from "@store/store.config";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { User } from "@model/index";
import { UserCard } from "../../components/user/user-card";
import { getUsersState } from "@store/select/user.select";
import { dispatchMaker } from "@store/dispatch/user.dispatch";
import { LOAD_USER } from "@store/actions";

interface AppComponentProps {
  onLoadUsers: Function;
  users: Array<User>;
}
interface AppComponentState { }
class App extends React.Component<AppComponentProps, AppComponentState> {
  constructor(props:AppComponentProps) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.onLoadUsers();

  }

  componentDidUpdate(
    prevProps: AppComponentProps,
    prevState: AppComponentState
  ) { }

  public render(): JSX.Element {
    if (!Array.isArray(this.props.users)) {
      return <div></div>
    }
    return (
      <div >
        <div >
          <div >
            <Link  to={`/user/add`}>
              Add user
            </Link>s
          </div>
        </div>
        <div >
          {
            this.props.users.map(u => <UserCard key={`user__${u.id}`} datum={u} />)
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  { users }
    : AppState): { users: Array<User> } => ({
      users: getUsersState(users)
    });
const mapDispatchToProps = dispatch => ({
  onLoadUsers: (pagination: number) => dispatchMaker(LOAD_USER, 1)
});

const ConnectedProducts = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default ConnectedProducts;
