import { dispatchAddUser } from "../dispatch/user.dispatch";
import Axios, { AxiosPromise, AxiosResponse } from "axios";
import { DEMO_API } from "../../shared/const";
import {
  ApiResponse,
  User,
  AppAction,
  BaseApiResponse,
  ApiPayload,
  UpdatedUser
} from "@model/index";
import { dispatchGetUser } from "@store/dispatch/user.dispatch";

export const getUser = (
  id: number
): ((Dispatch: any) => Promise<AppAction>) => {
  const url = `${DEMO_API}users/${id}`;
  const req: AxiosPromise<BaseApiResponse<User>> = Axios.get(url);
  return dispatch =>
    req.then((resp: AxiosResponse<BaseApiResponse<User>>) =>
      dispatchGetUser(resp.data.data)
    );
};
export const updateUser = (
  payload: ApiPayload<User>
): ((Dispatch: any) => Promise<any>) => {
  const { datum } = payload;
  const url = `${DEMO_API}users/${datum.id}`;
  const req: AxiosPromise<BaseApiResponse<UpdatedUser>> = Axios.put(url, datum);
  return dispatch =>
    req.then((resp: AxiosResponse<BaseApiResponse<UpdatedUser>>) => {
      const { updatedAt: value, ...user } = <any>resp.data;
      dispatchGetUser(user);
    });
};

export const addUser = (
  payload: ApiPayload<User>
): ((Dispatch: any) => Promise<any>) => {
  const { datum } = payload;
  const url = `${DEMO_API}users`;
  const req: AxiosPromise<BaseApiResponse<UpdatedUser>> = Axios.post(
    url,
    datum
  );
  return dispatch =>
    req.then((resp: AxiosResponse<BaseApiResponse<UpdatedUser>>) => {
      const { createdAt: value, ...user } = <any>resp.data;
      dispatchAddUser(user);
    });
};
