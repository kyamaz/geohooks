import { USER_LOADED, USER_ERROR } from "./../actions/index";
import { store } from "../../index";
import { LOAD_USER, GET_USER, UPDATE_USER, ADD_USER } from "@store/actions";
import { StoreAction } from "@model/index";

export const dispatchLoadUser = payload =>
  store.dispatch({
    type: LOAD_USER,
    payload
  });
export const dispatchGetUser = payload =>
  store.dispatch({
    type: GET_USER,
    payload
  });

export const dispatchUpdateUser = payload =>
  store.dispatch({
    type: UPDATE_USER,
    payload
  });

export const dispatchAddUser = payload =>
  store.dispatch({
    type: ADD_USER,
    payload
  });

//EPIC
/* export const dispatchLoadUsers = payload => ({
  type: LOAD_USER,
  payload
});
export const dispatchLoadedUsers = payload => ({ type: USER_LOADED, payload });
export const dispatchErrorUsers = payload => ({ type: USER_ERROR, payload });
 */
export const dispatchMaker = (type: string, payload: any): StoreAction<any> =>
  store.dispatch({
    type,
    payload
  });
export const actionMaker = (type: string, payload: any): StoreAction<any> => ({
  type,
  payload
});
