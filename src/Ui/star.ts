import * as React from "react";
import styled from "styled-components";
interface StyledProps {
  color: string;
}

export const Star = styled.i.attrs({})<StyledProps>`
  display: inline-block;
  width: 20px;
  text-align: center;
  color: ${props => props.color};
  &::before {
    display: inline-block;
    content: "\\2605";
  }
`;
