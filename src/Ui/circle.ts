import * as React from "react";
import styled from "styled-components";
import { DEFAULT_COLOR } from "@shared/style";
interface StyledProps {
  size: number;
}

export const Circle = styled.div.attrs({ className: "s-circle" })<StyledProps>`
  height: ${props => props.size}px;
  width: ${props => props.size}px;
  background-color: ${DEFAULT_COLOR};
`;

export const MapCircle = styled(Circle)`
  position: absolute;
  bottom: -30px;
  z-index: 15;
`;
