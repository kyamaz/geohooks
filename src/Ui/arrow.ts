
import * as React from "react";
import styled from "styled-components";
interface StyledProps {}
export const ArrowDown = styled.div.attrs({})<StyledProps>`
top: 100%;
left: 0%;
border: solid transparent;
content: " ";
height: 0;
width: 0;
position: absolute;
pointer-events: none;
border-color: transparent;
border-top-color: white;
border-width: 15px;
`;