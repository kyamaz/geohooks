import * as React from "react";
import styled from "styled-components";

interface StyledProps {}


export const MenuBadge = styled.div.attrs({
  className: "menu-badge"
})<StyledProps>``;
export const BadgeLabel = styled.label.attrs({
  className: "label label-primary"
})<StyledProps>``;