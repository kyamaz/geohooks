export const BASE_URL: string = `https://reqres.in/`;
export const DEMO_API: string = `${BASE_URL}api/`;

//MAPBOX

export const MAP_STYLE: string =
  "mapbox://styles/kyamaz9999/cjqbytpztfijr2roafypmjnzb";
export const MAP_TOKEN: string =
  "pk.eyJ1Ijoia3lhbWF6OTk5OSIsImEiOiJjanEzaW84ejMxZjVhNDNtdW1nd3dwYmFtIn0.kjf8alke7iQOrcXxuMmrUQ";

// YELP
//workaround YELP CORS ISSUE
//https://stackoverflow.com/questions/50204385/yelp-api-axios-reactjs-how-to-write-code
//preprend https://cors-anywhere.herokuapp.com/
export const CLIENT_ID: string = "iWDJ1Gj7FgyA6rxsnuTmqg";
export const YELP_TOKEN: string =
  "p_FBV6s-u7lyPrVlV7UZJDPAP0f1RqPQ-THjWCmGUgM7AVSONwDPVzGEJk_7C8Bulo8K9q6t8cSVqR0OXPzrYjRtNETs1RtIb9WkSgCCYBkIZVz3UabLHYXpwCQjXHYx";
export const YELP_ENDPOINT: string =
  "https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/";

//YELP_ENDPOINT
export const SEARCH_BUSINESS: string = "businesses/search";
