import { centerMap, userPositionToProps } from "./location";
import { Map } from "mapbox-gl";

export function makeMarkerFormat(data: Array<any>): FormatedFeature {
  const features: Array<MapBoxFeature> = data.map(d => makeFeature(d));
  return {
    type: "FeatureCollection",
    features
  };
}

export interface FormatedFeature {
  type: "FeatureCollection";
  features: Array<MapBoxFeature>;
}
export interface YelpBusiness {
  id: string;
  alias: string;
  name: string;
  image_url: string;
  is_closed: boolean;
  url: string;
  review_count: number;
  categories: Array<{ alias: string; title: string }>;
  rating: number;
  coordinates: { latitude: number; longitude: number };
  transactions: Array<any>;
  price: string;
  location: {
    address1: string;
    address2: string;
    address3: string;
    city: string;
    zip_code: string;
    country: string;
    state: string;
    display_address: Array<string>;
  };
  phone: string;
  photos?: Array<string>;
  is_claimed?: boolean;
  display_phone?: string;
  distance?: number;
  hours?: Array<{
    hours_type: string;
    open: Array<{
      is_overnight: boolean;
      end: string;
      day: number;
      start: string;
    }>;
    is_open_now: boolean;
  }>;
}

export interface MapBoxFeature {
  type: "Feature";
  properties: {
    message: string;
    iconSize: [number, number];
    source?: YelpBusiness;
  };
  geometry: {
    type: "Point";
    coordinates: [number, number];
  };
}
export function makeFeature(object: YelpBusiness): MapBoxFeature {
  const {
    coordinates: { latitude, longitude }
  } = object;
  return {
    type: "Feature",
    properties: {
      message: object.name,
      iconSize: [60, 60],
      source: object
    },
    geometry: {
      type: "Point",
      coordinates: [longitude, latitude]
    }
  };
}
export function setMapData(data) {
  return makeMarkerFormat(data.businesses);
}

export function setMapboxToContext(map: Map, props) {
  props.onSetMapBox({ map }); //map mapbox instance available to parent
}
