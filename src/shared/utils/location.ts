import { Map } from "mapbox-gl";
import { last } from "rxjs/operators";

export interface GeoPosition {
  lat: number;
  lon: number;
}
export function getUserPosition(): Promise<GeoPosition> {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          const pos = {
            lon: position.coords.longitude,
            lat: position.coords.latitude
          };
          return resolve(pos);
        },
        function() {
          return handleLocationError(true, reject);
        }
      );
    } else {
      // Browser doesn't support Geolocation
      return handleLocationError(false, reject);
    }
  });
}
function handleLocationError(browserHasGeolocation: boolean, fn) {
  console.error("handle loc error");
  return fn("error getting position ");
}
export function centerMap(map: Map, cb: Function): void {
  getUserPosition().then(pos => {
    map.setCenter(pos);
    console.log(map);
    cb(pos);
  });
}

export function userPositionToProps(cb: Function): void {
  getUserPosition().then(pos => cb(pos));
}

export function makeGeoParams(geo: GeoPosition): string {
  const { lat, lon } = geo;
  return `latitude=${lat}&longitude=${lon}`;
}
export function geoIsSet(geo: GeoPosition) {
  return !!geo.lat && !!geo.lon;
}
