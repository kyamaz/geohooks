import { Map, GeolocateControl } from "mapbox-gl";
import MapboxGL from "mapbox-gl/dist/mapbox-gl.js";

export function displayNavigationControl(map: Map) {
  const nav = new MapboxGL.NavigationControl();
  map.addControl(nav, "bottom-right");
}
export function displayScaleControl(map: Map) {
  const scale = new MapboxGL.ScaleControl();
  map.addControl(scale);
}
const geoOptions = {
  options: {
    positionOptions: { enableHighAccuracy: true },
    trackUserLocation: true,
    showUserLocation: true
  }
};

export function userLocation(map: Map) {
  const geoLocate = new MapboxGL.GeolocateControl(geoOptions);
  map.addControl(geoLocate, "top-right");

  return geoLocate;
  //geoLocate.trigger(); //show user location on map load
}
export function setMapControls(map: Map): GeolocateControl {
  displayNavigationControl(map);
  displayScaleControl(map);
  return userLocation(map);
}
