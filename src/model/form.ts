import { type } from "os";

export interface FormControls {
  [prop: string]: InputState;
}

export interface FormUpdatePayload {
  name: string;
  value: any;
  validationType: Array<string>;
}
export interface FormsControlsRef {
  [key: string]: FormsControlState;
}
export interface InputState {
  value: string | number;
  valid: any;
  touched: boolean;
  validationType?: Array<ValidationKey>;
  errorDisplay: string;
}
export interface FormsControlState {
  state: InputState;
  setter: Function;
  validations: Array<ValidationKey>;
}
export interface ValidateState {
  value: boolean;
  message: string;
}

export type LoginType = "signin" | "signup";
export interface AuthPayload {
  login: string;
  pwd: string;
}

export type ValidationKey = "required" | "minLength" | "isEqual" | "isUrl";
//https://github.com/Microsoft/TypeScript/issues/24220
export type ErrorMessagesMap = { [key in ValidationKey]: string };
