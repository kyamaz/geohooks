import * as React from "react";
import { useContext, Fragment } from "react";
import { MapContext } from "src/context/mapContext";
import { MapBoxFeature, setMapboxToContext } from "@shared/utils/mapbox";
import ReactMapboxGl, { Marker } from "react-mapbox-gl";
import { MAP_TOKEN, MAP_STYLE } from "@shared/const";
import { Map } from "mapbox-gl";
import { MapToolTip } from "@components/MapTooltip/MapTooltip";
import * as PropTypes from "prop-types";
import { AppContext } from "src/context/AppContext";


function renderFeatures(features){
  return features.map((datum: MapBoxFeature, index: number) => {
    return (
      <Marker
        key={datum.properties.source.id}
        coordinates={datum.geometry.coordinates}
      >
        <MapToolTip datum={datum.properties.source} />
      </Marker>
    )})
}


export interface MapboxApiProps {
  onSetMapBox?: Function;
}

// call ReactMapboxGl outside rendering fn else instancied every time
const MapBox = ReactMapboxGl({
  accessToken: MAP_TOKEN
});
function _init(map: Map, props: MapboxApiProps): void {
  setMapboxToContext(map, props);
}
const mapboxApi = (props: MapboxApiProps): JSX.Element => {
  ///context
  const context: any = useContext(MapContext);
  const appontext: any = useContext(AppContext);
console.log(appontext)
  const { data, position } = context;
  const features = data ? data.features : [];
  //init
  const mapLoaded = (map: Map) => _init.call(null, map, props);
  console.log("render", context);

  const memoizedFeatures = React.useMemo(() =>renderFeatures(features) , [features])


  return (
    <MapBox
      style={MAP_STYLE}
      containerStyle={{
        height: "100vh",
        width: "100vw"
      }}
      center={[position.lon, position.lat]}
      zoom={[14]}
      onStyleLoad={mapLoaded}
    >
      {memoizedFeatures}
    </MapBox>
  );
};

mapboxApi.propTypes = {
  onSetMapBox: PropTypes.func,
  onSetUserPosition: PropTypes.func
};
const MemoMapboxApi = React.memo((props: any) => mapboxApi(props));
export { MemoMapboxApi as MapboxApi };
