import * as React from 'react'
import { User } from '@model/index';
import { Link } from 'react-router-dom';

interface Props {
  datum: User
}
const user = (props: Props): JSX.Element => {
  return (
    <div >

      <div >
        <div >
          <img src={props.datum.avatar}  />
        </div>
        <div >
          <div >{props.datum.first_name} {props.datum.last_name} </div>
        </div>
        <div >
          <p>some description </p>
        </div>
        <div >
          <Link 
            to={`/user/${props.datum.id}`} >
            update
          </Link>
        </div>
      </div>
    </div>

  )
}

export { user as UserCard }