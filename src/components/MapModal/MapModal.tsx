import * as React from "react";

import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";

import { useModalInit } from "@hooks/useModalInit";
import * as PropTypes from "prop-types";

interface MapModalProps {
  isExpanded?: boolean;
  onCloseModal?: Function;
  from?: 'left'| 'bottom' | 'right';
  width?:string;
  children?: React.ReactNode;
}

function mapModal(props: MapModalProps, ref: any) {
  const togglePane = (e: React.SyntheticEvent) => {
    e.preventDefault();
    props.onCloseModal(false);
  };

  const modal = React.useRef(null);
  useModalInit(modal);
  const { from, isExpanded, width } = props;

  return (
    <div ref={modal}>
      <SlidingPane
        isOpen={isExpanded}
        onRequestClose={togglePane}
        from={from}
        width={width}
        overlayClassName="zIndex"
      >
        {props.children}
      </SlidingPane>
    </div>
  );
}
mapModal.propTypes = {
  onCloseModal: PropTypes.func
};

export { mapModal as MapModal };
