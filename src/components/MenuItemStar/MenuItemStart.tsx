import * as React from "react";
import { Star } from "src/Ui/star";
import { MenuItem } from "src/Ui/menu";
import { DEFAULT_COLOR } from "@shared/style";



export function menuItemStar(props: {
  label: string;
  value: any;
}): JSX.Element {
  const { label, value } = props;
  const stars = [1, 2, 3, 4, 5];
  return (
    <MenuItem>
      <span>{label}</span>
      {stars.map(s => (
        <Star
          key={`star_${s}`}
          color={value >= s ? `${DEFAULT_COLOR}` : "transparent"}
        />
      ))}
    </MenuItem>
  );
}
export { menuItemStar as MenuItemStar };
