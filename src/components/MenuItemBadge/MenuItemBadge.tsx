import * as React from "react";
import { MenuItem } from "src/Ui/menu";
import { MenuBadge, BadgeLabel } from "src/Ui/badge";

export function menuItemBadge(props: {
  label: string;
  value: any;
}): JSX.Element {
  const { label, value } = props;

  return (
    <MenuItem>
      <span>{label}</span>
      <MenuBadge>
        <BadgeLabel>{value}</BadgeLabel>
      </MenuBadge>
    </MenuItem>
  );
}
export { menuItemBadge as MenuItemBadge };
