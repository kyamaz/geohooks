import * as React from "react";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { NavHeader, NavSection, NavLinkItem } from "src/Ui/navbar";

function appNav(props: any): JSX.Element {
  return (
    <NavHeader>
      <NavSection>
        <NavLinkItem exact={true} to="/">
          home
        </NavLinkItem>
        <NavLinkItem exact={true} to="/map">
          map
        </NavLinkItem>
      </NavSection>
    </NavHeader>
  );
}

export { appNav as AppNav };
