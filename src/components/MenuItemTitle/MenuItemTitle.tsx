import * as React from "react";
import styled from "styled-components";
import { MenuItem } from "src/Ui/menu";
import { DEFAULT_COLOR } from "@shared/style";

interface StyledProps {}

const TooltipTitle = styled.a.attrs({})<StyledProps>`
  text-align: center;
  color: ${DEFAULT_COLOR};
`;

export function menuItemTitle(props: {
  name: string;
  url: string;
}): JSX.Element {
  const { name, url } = props;
  return (
    <MenuItem>
      <TooltipTitle href={url}>{name}</TooltipTitle>
    </MenuItem>
  );
}
export { menuItemTitle as MenuItemTitle };
