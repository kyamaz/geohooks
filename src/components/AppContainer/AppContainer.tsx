import * as React from "react";
import { Route } from "react-router-dom";
import { AppNav } from "@components/app-nav/app-nav";

const appContainer = ({ component: Component, ...rest }) => {
  const { path } = rest;
  return (
    <Route
      {...rest}
      render={matchProps => (
        <div>
          <AppNav />
        
            <Component {...matchProps} />
        </div>
      )}
    />
  );
};

export {appContainer as AppContainer}