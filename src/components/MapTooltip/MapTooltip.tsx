import * as React from "react";
import styled from "styled-components";
import { YelpBusiness } from "@shared/utils/mapbox";
import { MenuItemBadge } from "@components/MenuItemBadge/MenuItemBadge";
import { MenuItemTitle } from "@components/MenuItemTitle/MenuItemTitle";
import { MenuItemStar } from "@components/MenuItemStar/MenuItemStart";
import { MenuItemCategories } from "@components/MenuItemCategories/MenuItemCategories";
import { MenuItemDistance } from "@components/MenuItemDistance/MenuItemDistance";
import { useState } from "react";
import {  MapCircle } from "src/Ui/circle";
import { MenuDivider } from "src/Ui/divider";
import { ArrowDown } from "src/Ui/arrow";
import { ListUnstyled } from "src/Ui/list";
interface StyledProps {
  isExpanded: boolean;
}

const TooltipContainer = styled.ul.attrs({
  className: "menu"
})<StyledProps>`
  position: relative;
  min-width: ${props => (props.isExpanded ? "1OO%" : "60px")};
  overflow: ${props => (props.isExpanded ? "hidden" : "visible")};
  white-space: ${props => (props.isExpanded ? "nowrap" : "normal")};
`;

export function mapToolTip(props: { datum?: YelpBusiness }): JSX.Element {
  const [isExpanded, setExpanded] = useState(false);
  const {
    datum: { name, price, rating, url, distance, categories }
  } = props;
  const handleToggle = (e: React.SyntheticEvent) => {
    e.preventDefault();
    setExpanded(!isExpanded);
  };
  const close = (e: React.SyntheticEvent) => {
    e.preventDefault();
    setExpanded(false);
  };

  const menuDetail: JSX.Element = (
    <ListUnstyled>
      <MenuDivider />
      <MenuItemCategories label="categories" value={categories} />
      <MenuItemBadge label="rating" value={rating} />
      <MenuItemStar label="price" value={!!price ? price.length : 0} />
      <MenuItemDistance label="distance" value={distance} />
    </ListUnstyled>
  );
  return (
    <TooltipContainer
      onClick={handleToggle}
      onBlur={close}
      isExpanded={isExpanded}
    >
      <MenuItemTitle name={name} url={url} />
      {isExpanded ? menuDetail : null}
      <ArrowDown />
      <MapCircle size={10} />
    </TooltipContainer>
  );
}
export { mapToolTip as MapToolTip };
