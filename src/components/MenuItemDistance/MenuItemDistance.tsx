import * as React from "react";
import styled from "styled-components";
import { distToMin } from "@shared/utils/helper";
import { MenuItem, MenuLabel, MenuValue } from "src/Ui/menu";

export function menuItemDistance(props: {
  label: string;
  value: any;
}): JSX.Element {
  const { label, value } = props;

  return (
    <MenuItem>
      <MenuLabel>{label} </MenuLabel>
      <MenuValue>{distToMin(value)} min. </MenuValue>
    </MenuItem>
  );
}
export { menuItemDistance as MenuItemDistance };
