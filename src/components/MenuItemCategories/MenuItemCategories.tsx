import * as React from "react";
import styled from "styled-components";
import { MenuItem } from "src/Ui/menu";
import { Chip } from "src/Ui/chip";

interface YelpBusinessCat {
  alias: string;
  title: string;
}

export function menuItemCategories(props: {
  label: string;
  value: Array<YelpBusinessCat>;
}): JSX.Element {
  const { label, value } = props;
  return (
    <MenuItem>
      {value.map((v: YelpBusinessCat) => (
        <Chip key={v.alias}>{v.title}</Chip>
      ))}
    </MenuItem>
  );
}
export { menuItemCategories as MenuItemCategories };
