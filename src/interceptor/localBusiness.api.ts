import { dispatchIsLoading } from "./../store/dispatch/app.dispatch";
import { YELP_ENDPOINT, YELP_TOKEN } from "./../shared/const";
import axios from "axios";
import { AppToken } from "../state/state";
axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    console.log("default req");
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

let instance = axios.create({
  baseURL: YELP_ENDPOINT,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json;"
  }
});

instance.interceptors.request.use(
  config => {
    const t = AppToken.get();
    console.log(t);
    dispatchIsLoading(true);
    config.headers.Authorization = `Bearer ${YELP_TOKEN}`;
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  response => {
    // Do something with response data
    dispatchIsLoading(false);
    return response;
  },
  error => {
    // Do something with response error
    dispatchIsLoading(false);

    console.log(error);
    return Promise.reject(error);
  }
);

export { instance as YELP_API };
